require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get "/"
    assert_response :success
  end

  test "should have a 'container' div" do
    get "/"
    assert_select "div.container"
  end

  test "should have a 'wrapper' class" do
    get "/"
    assert_select ".wrapper"
  end

  test "should have only one 'h1' " do
    get "/"
    assert_select "h1", :count => 1
  end

  test "should have a 'tabs' class item" do
    get "/"
    assert_select ".tabs"
  end

  test "'tabs' class should have several 'tab' class items" do
    get "/"
    assert_select ".tabs" do
      assert_select ".tab", :minimum => 3
    end
  end

  test "should have a javascript script" do
    get "/"
    assert_select "script"
  end

end
