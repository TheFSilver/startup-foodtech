# <a href="https://the-foodtech.herokuapp.com/" target="_blank">The FoodTech Startup</a>

Cliquez <a href="https://the-foodtech.herokuapp.com/" target="_blank">ici</a> pour visiter le site internet de la <a href="https://the-foodtech.herokuapp.com/" target="_blank">FoodTech Startup</a>:

## Instructions: ##

Voici quelques commandes à taper dans votre terminal pour installer le repository en local, sans accroc:
```
cd Desktop
git clone https://gitlab.com/TheFSilver/startup-foodtech.git
cd startup-FoodTech
bundle install
rails db:create
rails db:migrate
rails server
```

Visitez donc la page <a href="http://localhost:3000/" target="_blank">Localhost:3000</a> pour visualier le site en local.

## Contributeur: ##
Fait par François D. aka <a href="https://github.com/TheFSilver" target="_blank">TheFSilver</a>
